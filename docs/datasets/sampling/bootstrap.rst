olympus.datasets.sampling.bootstrap module
==========================================

.. automodule:: olympus.datasets.sampling.bootstrap
    :members:
    :undoc-members:
    :show-inheritance:
