olympus.datasets.sampling.balanced\_classes module
==================================================

.. automodule:: olympus.datasets.sampling.balanced_classes
    :members:
    :undoc-members:
    :show-inheritance:
