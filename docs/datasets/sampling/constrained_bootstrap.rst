olympus.datasets.sampling.constrained\_bootstrap module
=======================================================

.. automodule:: olympus.datasets.sampling.constrained_bootstrap
    :members:
    :undoc-members:
    :show-inheritance:
