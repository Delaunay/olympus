Datasets
========

Subpackages
-----------

.. toctree::

    sampling/index

Submodules
----------

.. toctree::

   archive
   cifar10
   cifar100
   dataset
   pennfudan
   emnist
   fashionmnist
   gaussian
   group
   mnist
   svhn
   tensorhdf5
   tinyimagenet
   transform
