olympus.tasks.detection module
==============================

.. automodule:: olympus.tasks.detection
    :members:
    :undoc-members:
    :show-inheritance:
