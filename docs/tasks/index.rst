Tasks
=====

Submodules
----------

.. toctree::

   classification
   detection
   gan
   rl
   segmentation
   task

Module contents
---------------

.. automodule:: olympus.tasks
    :members:
    :undoc-members:
    :show-inheritance:
