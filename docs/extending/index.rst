Extending Olympus
=================



.. toctree::
   :caption: Writing custom blocks
   :maxdepth: 1

   custom_model
   custom_model_nas
   custom_optimizer
   custom_schedule
   custom_observer
