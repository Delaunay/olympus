Optimizers
==========

Registered Optimizers
---------------------

.. toctree::

   adam
   amsgrad
   base
   sgd


.. toctree::
   :caption: Schedules
   :maxdepth: 1

   schedules/index


Module contents
---------------

.. automodule:: olympus.optimizers
    :members:
    :undoc-members:
    :show-inheritance:
