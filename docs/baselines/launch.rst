olympus.baselines.launch module
===============================

.. automodule:: olympus.baselines.launch
    :members:
    :undoc-members:
    :show-inheritance:
