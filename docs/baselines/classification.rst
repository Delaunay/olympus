olympus.baselines.classification module
=======================================

.. automodule:: olympus.baselines.classification
    :members:
    :undoc-members:
    :show-inheritance:
