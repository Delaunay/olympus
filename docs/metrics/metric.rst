Metric Interface
================

.. automodule:: olympus.metrics.metric
    :members:
    :undoc-members:
    :show-inheritance:
