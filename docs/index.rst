.. include:: ../README.rst


.. toctree::
   :caption: Getting Started

   start/start.rst

.. toctree::
   :caption: Examples

   examples/minimalist
   examples/hpo_simple


.. toctree::
   :caption: Extending

   extending/index


.. toctree::
   :caption: Contributing

   contributing/index


.. toctree::
   :caption: API
   :maxdepth: 1

   accumulators/index
   datasets/index
   distributed/index
   hpo/index
   metrics/index
   observers/index
   models/index
   optimizers/index
   reinforcement/index
   baselines/index
   tasks/index
   utils/index

