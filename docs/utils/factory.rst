olympus.utils.factory module
============================

.. automodule:: olympus.utils.factory
    :members:
    :undoc-members:
    :show-inheritance:
