olympus.utils.dtypes module
===========================

.. automodule:: olympus.utils.dtypes
    :members:
    :undoc-members:
    :show-inheritance:
