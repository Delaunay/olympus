olympus.utils.stat module
=========================

.. automodule:: olympus.utils.stat
    :members:
    :undoc-members:
    :show-inheritance:
