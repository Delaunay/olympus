from .group import GroupPage
from .project import ProjectPage
from .trial import TrialPage
from .debug import DebugPage